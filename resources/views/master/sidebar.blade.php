<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('dist/img/dk2.png') }}" alt="Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8; margin-left: 0px">
        <span class="brand-text font-weight-light">Dividend Kingdom</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <i class="fas fa-user fa-3x"></i>
                {{--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">--}}
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                {{--<a href=" {{ route('logout') }}" class="d-block" style="margin-left: 10px">Logout</a>--}}
                <a href="{{ route('logout') }}"
                   class="d-block"
                   style="margin-left: 10px"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                   Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
            <div class="info">

            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        @if(! \Illuminate\Support\Facades\Auth::user()->scanner)
                            <span class="badge badge-warning float-left" style="margin-left: -8px; margin-right: 10px">Demo</span>
                        @endif
                        {{--<i class="nav-icon fa fa-filter"></i>--}}
                            <i class="fas fa-search nav-icon"></i>
                        <p>
                            Scanner
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('home')}}" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('scanner-filter')}}" class="nav-link">
                                <i class="fa fa-filter nav-icon"></i>
                                <p>Filter</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('scanner-charts')}}" class="nav-link">
                                <i class="nav-icon fas fa-chart-bar"></i>
                                <p>Historical analysis</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <img class="nav-icon" src="{{asset('dist/img/area-graph.svg')}}" alt="">
                        <p>
                            Growing Dividends
                            <i class="right fas fa-angle-left"></i>
                            {{--<img class="nav-icon" src="{{asset('dist/img/increasing-stocks-graphic.png')}}" alt="">--}}
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('growingDividends:calendar') }}" class="nav-link">
                                <i class="far fa-calendar-alt nav-icon"></i>
                                <p>Dividend Calendar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('growingDividends:timeline') }}" class="nav-link">
                                <i class="nav-icon fas fa-clock"></i>
                                <p>Timeline</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('growingDividends:portfolio') }}" class="nav-link">
                                <i class="nav-icon fas fa-wallet"></i>
                                <p>Assets</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-comment-dollar fa-2x"></i>
                        <p>
                            Accountant
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        {{--<li class="nav-item">--}}
                            {{--<a href="#" class="nav-link">--}}
                                {{--<i class="far fa-circle nav-icon"></i>--}}
                                {{--<p>Dashboard (soon)</p>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                    </ul>
                </li>
            </ul>

            {{--<ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">--}}
            {{--</ul>--}}
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>