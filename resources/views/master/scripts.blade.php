<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
{{--<script>--}}
    {{--$.widget.bridge('uibutton', $.ui.button)--}}
{{--</script>--}}
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
{{--<script src="plugins/chart.js/Chart.min.js"></script>--}}
<!-- Sparkline -->
{{--<script src="plugins/sparklines/sparkline.js"></script>--}}
<!-- JQVMap -->
{{--<script src="plugins/jqvmap/jquery.vmap.min.js"></script>--}}
{{--<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>--}}
<!-- jQuery Knob Chart -->
{{--<script src="plugins/jquery-knob/jquery.knob.min.js"></script>--}}
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
{{--<script src="plugins/daterangepicker/daterangepicker.js"></script>--}}
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
{{--<script src="plugins/summernote/summernote-bs4.min.js"></script>--}}
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="dist/js/pages/dashboard.js"></script>--}}
<!-- AdminLTE for demo purposes -->
{{--<script src="dist/js/demo.js"></script>--}}

<script>

    function setActiveNavLink()
    {
        var links = $('a.nav-link')
        var uri = window.location.href

        for (i = 0; i < links.length; i++) {
            if (links[i].href === uri) {
                links[i].classList.add('active')
            }
        }
        x = $("a[href$='" + uri + "']").parents("li")
        x[1].classList.add('menu-open')
        x[1].firstElementChild.classList.add('active')

    }
    $(document).ready( function () {
        setActiveNavLink()
    });
</script>

@yield('scripts')