<!DOCTYPE html>
<html>
@include('master.head')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@include('master.navbar')

@include('master.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> {{ (isset($title)) ? $title : ''}}</h1>
                    </div><!-- /.col -->
                    {{--<div class="col-sm-6">--}}
                        {{--<ol class="breadcrumb float-sm-right">--}}
                            {{--<li class="breadcrumb-item"><a href="#">Home</a></li>--}}
                            {{--<li class="breadcrumb-item active">Dashboard v1</li>--}}
                        {{--</ol>--}}
                    {{--</div><!-- /.col -->--}}
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @yield('content')

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2019 <a href="https://usstocks.pl/">USStocks</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 1.0-beta
        </div>
    </footer>

    <!-- Control Sidebar -->
    {{--<aside class="control-sidebar control-sidebar-dark">--}}
        {{--<!-- Control sidebar content goes here -->--}}
    {{--</aside>--}}
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('master.scripts')
</body>
</html>
