<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<style type="text/css">
    table tr td:not(:first-child) {
        text-align: right;
    }
    th:not(:first-child) {
        text-align: right;
    }
</style>
<head>
</head>

<body>

<pre>

</pre>
<p><strong> {{  strtoupper($ticker) }} </strong> </p>
<table>
    <tr>
        <th> Annual Data </th>
    @foreach ($annuals as $key => $row)
            <th> {{ $row['fy'] }} </th>
    @endforeach
    </tr>


    <tr>
        <td>
            Total Revenue:
        </td>
        @foreach ($annuals as $row)
        <td>
            {{ number_format($row['revenue'],  2, $number_format["decimal"], $number_format["thousands"]) }}
        </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EBITDA:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['ebitda'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Interest Income:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['interest_income'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EBIT:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['ebit'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Interest Expense:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['interest_expense'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total Net Income:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['net_income'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total Net Income ex OF:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['net_income_ex_nri'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EPS basic:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['eps_basic'],  2, $number_format["decimal"], $number_format["thousands"])}}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EPS diluted:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['eps_diluted'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EPS ex OF, diluted:
        </td>
        @foreach ($annuals as $row)
            <td>
                @if ($row['eps_basic'] !== 0.0)
                    {{ number_format($row['eps_without_nri'] * $row['eps_diluted'] / $row['eps_basic'],  2, $number_format["decimal"], $number_format["thousands"])}}
                @else
                    0
                @endif
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Dividend:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['dividends_per_share'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash, equivalents and marketable securities:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['cash_equivalents_and_marketable_securities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Inventories:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_inventories'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total current assets:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_current_assets'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total assets:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_assets'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Short term debt & lease obligations:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['short_term_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Long term debt & lease obligations:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['long_term_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total debt & lease obligations:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total current liabilities:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_current_liabilities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total long term liabilities:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_long_term_liabilities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total liabilities:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_liabilities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total equity:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['total_equity'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Shares outstanding:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['shares_outstanding_diluted'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash from operations:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['cashflow_from_operations'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash from investing:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['cashflow_from_investing'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Net debt issuance:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['net_issuance_of_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Net Stocks Repurchase:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['net_stocks_repurchase'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash from financing:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['cashflow_from_financing'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Stock price:
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['month_end_stock_price'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Date:
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            {{ $row['estimate_date1'] }}
        </td>
        <td>
            {{ $row['estimate_date2'] }}
        </td>
        <td>
            {{ $row['estimate_date3'] }}
        </td>
    </tr>
    <tr>
        <td>
            Estimates:
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            {{ number_format($row['estimate_eps1'],  2, $number_format["decimal"], $number_format["thousands"]) }}
        </td>
        <td>
            {{ number_format($row['estimate_eps2'],  2, $number_format["decimal"], $number_format["thousands"]) }}
        </td>
        <td>
            {{ number_format($row['estimate_eps3'],  2, $number_format["decimal"], $number_format["thousands"]) }}
        </td>
    </tr>

    <tr>
        <td>
            Free Cash Flow
        </td>
        @foreach ($annuals as $row)
            <td>
                {{ number_format($row['free_cash_flow'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
{{--@endforeach--}}
</table>
<br><br>
<table>
    <tr>
        <th>Quarterly Data </th>
        @foreach ($quarters as $key => $row)
            <th> {{ $row['fy'] }} </th>
        @endforeach
    </tr>


    <tr>
        <td>
            Total Revenue:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['revenue'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EBITDA:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['ebitda'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Interest Income:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['interest_income'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EBIT:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['ebit'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Interest Expense:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['interest_expense'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total Net Income:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['net_income'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total Net Income ex OF:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['net_income_ex_nri'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EPS basic:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['eps_basic'],  2, $number_format["decimal"], $number_format["thousands"])}}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EPS diluted:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['eps_diluted'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            EPS ex OF, diluted:
        </td>
        @foreach ($quarters as $row)
            <td>
                @if ($row['eps_basic'] !== 0.0)
                    {{ number_format($row['eps_without_nri'] * $row['eps_diluted'] / $row['eps_basic'],  2, $number_format["decimal"], $number_format["thousands"])}}
                @else
                    0
                @endif
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Dividend:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['dividends_per_share'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash, equivalents and marketable securities:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['cash_equivalents_and_marketable_securities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Inventories:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_inventories'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total current assets:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_current_assets'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total assets:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_assets'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Short term debt & lease obligations:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['short_term_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Long term debt & lease obligations:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['long_term_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total debt & lease obligations:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total current liabilities:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_current_liabilities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total long term liabilities:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_long_term_liabilities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total liabilities:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_liabilities'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Total equity:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['total_equity'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Shares outstanding:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['shares_outstanding_diluted'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash from operations:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['cashflow_from_operations'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash from investing:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['cashflow_from_investing'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Net debt issuance:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['net_issuance_of_debt'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Net Stocks Repurchase:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['net_stocks_repurchase'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Cash from financing:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['cashflow_from_financing'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    <tr>
        <td>
            Stock price:
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['month_end_stock_price'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>

    <tr>
        <td>
            Free Cash Flow
        </td>
        @foreach ($quarters as $row)
            <td>
                {{ number_format($row['free_cash_flow'],  2, $number_format["decimal"], $number_format["thousands"]) }}
            </td>
        @endforeach
    </tr>
    {{--@endforeach--}}
</table>
</body>

</html>
