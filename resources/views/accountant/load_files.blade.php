{{--@extends('layouts.app')--}}
{{--@section('scripts/css')--}}
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>USSoft</title>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/component.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
        .inputfile + label {
            font-size: 1.25em;
            font-weight: 700;
            color: white;
            background-color: #209cee;
            display: inline-block;
        }

        .inputfile:focus + label,
        .inputfile + label:hover {
            background-color: #367cd3;
        }

        .btn1 {
            background-color: #367cd3;
        }

        .button.is-info {
            width: 100%;
        }

        img {
            margin-left: auto;
            margin-right: auto;
        }
        li, p {
            margin-top: 8px;
            margin-bottom: 8px;
        }
        #disclousure {
            position: absolute;
            bottom: 0;
            right: 15px;
        }

    </style>

</head>
<body>
{{--@endsection--}}

{{--@section('content')--}}

<section class="hero">
    <div class="hero-body">
        <div class="container">
            <figure class="image center">
                <img src="usstocks.png" style="max-width: 480px;">
            </figure>

        </div>
    </div>
</section>

<div class="section">
    <div class="container">
        <div class="tile is-ancestor">
            <div class="tile is-vertical">
                <div class="tile">
                    <div class="tile is-parent is-vertical is-3">

                        <form method="post" enctype="multipart/form-data" action="{{dashboard}}">
                            {{ csrf_field() }}
                            <article class="tile is-child ">
                                <h1 class="title">Akcje</h1>
{{--                                <div class="file">--}}
                                    <input type="file" name="akcje" id="akcje" class="inputfile inputfile-1" />
                                    <label for="akcje">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                             viewBox="0 0 20 17">
                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                        </svg>
                                        <span>Akcje&hellip;</span>
                                    </label>
{{--                                </div>--}}
                                <br>
                                {{--Lub <a href="{{route('merge')}}">Scal pliki z akcjami</a>--}}
                            </article>

                            <article class="tile is-child ">
                                <h1 class="title">Dywidendy</h1>
                                <div class="file">
                                    <input type="file" name="cash" id="cash" class="inputfile inputfile-1"
                                           data-multiple-caption="{count} files selected" multiple/>
                                    <label for="cash">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
                                             viewBox="0 0 20 17">
                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                                        </svg>
                                        <span>Dywidendy&hellip;</span>
                                    </label>
                                </div>
                                <br>
                            </article>

                            <article class="tile is-child ">
                                <h1 class="title">Wybierz zakres dat</h1>
                                <div id="reportrange" name="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <input type="hidden" name="daterange" value="">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span></span> <i class="fa fa-caret-down"></i>
                                </div>
                                <br>
                            </article>

                            <article class="tile is-child ">
                                <input type="submit" class="button is-info " value="Raport">
                            </article>
                        </form>
                    </div>
                    <div class="tile is-parent">
                        <article class="tile is-child box notification is-info">
                                <h1 class="title">Ważne informacje</h1>
                            <p>
                                Z systemu raportowego Intercative Brokers pobieramy 2 raporty, na podstawie których zostaną
                                wykonane wszystkie dalsze przeliczenia. Pierwszy raport to raport dotyczący akcji, futures i transakcji
                                opcyjnych. Drugi raport będzie dotyczył samych dywidend.
                                Ważne jest, żeby pobrane raporty zawierały ściśle określone dane dlatego postępuj dalej zgodnie z tą
                                {{--<a href="{{route('instructions')}}">instrukcją!</a>--}}

                            <p><strong>Pamiętaj również, że do uzyskania poprawnego wyniku dotyczącego akcji potrzebujesz pełnej historii transakcji - pobranie transakcji tylko z danego okresu skutkować będzie błędnymi wyliczeniami!</strong></p>
                            </p>
                            <p>
{{--                                Jeżeli masz wątpliwości, w jaki sposób uzyskane zostały prezentowane wyniki, zapoznaj się z <a href="{{route ('methodology')}}">metodologią</a> obliczeń--}}
                            </p>
                            <p id="disclousure">
                                <strong>Korzystając z aplikacji zgadzasz się z postanowieniami <a href="#">regulaminu</a></strong>
                            </p>

                        </article>
                    </div>
                </div>
                <div class="tile is-parent">
                    <article class="tile is-child ">

                    </article>
                </div>
            </div>
        </div>
    </div>
</div>

{{--@endsection--}}

{{--@section('scripts')--}}
<script src="js/jquery.custom-file-input.js"></script>
<script type="text/javascript">
    $(function() {

        var start = moment().subtract(1, 'year').startOf('year');
        var end = moment().subtract(1, 'year').endOf('year');

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            $('#reportrange input[type="hidden"]').val(start.format('YYYY-MM-DD') + ' : ' + end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            locale: {
                "customRangeLabel": "Inny okres",
                "applyLabel": "Potwierdź",
                "cancelLabel": "Anuluj",
            },
            startDate: start,
            endDate: end,
            ranges: {
                'Poprzedni rok': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
                'Ostatnie 3 mce': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Poprzedni miesiąc': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Ostatnie 30 dni': [moment().subtract(29, 'days'), moment()],
            }
        }, cb);

        cb(start, end);

    });
</script>
{{--@endsection--}}

</body>
</html>