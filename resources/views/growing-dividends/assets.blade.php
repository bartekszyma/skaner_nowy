@extends('master.layout')

@section('style')
    <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="   crossorigin="anonymous"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
{{--@php--}}
    {{----}}
{{--@endphp--}}
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Growing Dividends Portfolio</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {{--<table id="example1" class="table table-bordered table-striped">--}}
                    {{--<table id="y_daily" class="table table-bordered table-striped">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>Ticker</th>--}}
                            {{--<th>I</th>--}}
                            {{--<th>II</th>--}}
                            {{--<th>III</th>--}}
                            {{--<th>IV</th>--}}
                            {{--<th>V</th>--}}
                            {{--<th>VI</th>--}}
                            {{--<th>VII</th>--}}
                            {{--<th>VIII</th>--}}
                            {{--<th>IX</th>--}}
                            {{--<th>X</th>--}}
                            {{--<th>XI</th>--}}
                            {{--<th>XII</th>--}}

                        {{--</tr>--}}
                        {{--</thead>--}}
                    {{--</table>--}}
                    {!!  $table !!}
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready( function () {
            var x = $('#tableCalendar').DataTable({
                {{--lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],--}}
                {{--// columnsDef: [--}}
                {{--//     { "width": "10px", "targets": 0 },--}}
                {{--//     { "width": "35%", "targets": 1 },--}}
                {{--//     { "width": "10%", "targets": 3 },--}}
                {{--//     ],--}}
                {{--// columns: [--}}
                {{--//     { "width" : "5px"},--}}
                {{--// ],--}}
                {{--ajax: {--}}
                    {{--url: '{{url('/yahoo_daily')}}',--}}
                    {{--dataSrc: ''--}}
                {{--},--}}
                {{--columns: [--}}
                    {{--{ "data": "Ticker" },--}}
                    {{--{ "data": "I" },--}}
                    {{--{ "data": "II" },--}}
                    {{--{ "data": "III"},--}}
                    {{--{ "data": "IV"},--}}
                    {{--{ "data": "V"},--}}
                    {{--{ "data": "VI"},--}}
                    {{--{ "data": "VII"},--}}
                    {{--{ "data": "VIII" },--}}
                    {{--{ "data": "IX"},--}}
                    {{--{ "data": "X" },--}}
                    {{--{ "data": "XI"},--}}
                    {{--{ "data": "XII" },--}}

                {{--]--}}
            });
            //deals with resizing
            {{--x.ajax.url("{{url('/yahoo_daily')}}").load()--}}
            // x.fnAdjustColumnSizing();// https://legacy.datatables.net/ref#fnAdjustColumnSizing
            // setTimeout( x.ajax.reload, 5000)

            {{--document.getElementById('filters').addEventListener('submit', (event) => {--}}
                {{--event.preventDefault()--}}
                {{--var formElements = document.getElementById("filters").elements--}}
                {{--var get = ''--}}
                {{--for (i = 0; i < formElements.length - 1; i++) {--}}
                    {{--get += formElements[i].name + '=' + formElements[i].value + '&'--}}
                {{--}--}}
                {{--get = get.substring(0, get.length - 1)--}}
                {{--x.ajax.url("{{url('/yahoo_daily') . '/?'}}" + get).load()--}}

            {{--})--}}
            // function restoreDefaults(){
            //     document.getElementById('NoYrs').value = 10;
            // }
            // $("#restore").click(function() {
            //     $('#filters').trigger('reset');
            // })
            // $("#offAll").click(function() {
            //     $('form#filters :input').each(function() {
            //         $(this).val('off');
            //     });
            // })
            // $('.nav-link').click(function() {
            //     console.log('klikles')
            //     $('.nav-link').removeClass('active')
            //     $(this).addClass('active')
            // });
            // var g = $('form')
            // console.log(g)

        } );
    </script>

    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endsection
