@extends('master.layout')

@section('content')
    <!-- Timelime example  -->
    @for($i = 0; $i < count($charts); $i++)
        @if ($i%2 === 0)
    <div class="row">
        @endif
        <div class="col-md-6">
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        {{$charts[$i]['title']}}
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chartjs-wrapper">
                        <canvas id="chartjs-{{$charts[$i]['id']}}" class="chartjs" width="undefined" height="100"></canvas>
                    </div>

                </div>
                <!-- /.card-body-->
            </div>
        </div>
        @if ($i%2 === 0)
    </div>
        @endif
    @endfor
    <div class="row">
        <div class="col-md-6">
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        Line Chart
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {{--<div id="line-chart" style="height: 300px;"></div>--}}
                    {{--<canvas id="myChart" width="400" height="200"></canvas>--}}
                    <div class="chartjs-wrapper">
                        <canvas id="chartjs-roe2" class="chartjs" width="undefined" height="100"></canvas>
                    </div>

                </div>
                <!-- /.card-body-->
            </div>
        </div>
        <div class="col-md-6">
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        Line Chart
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {{--<div id="line-chart" style="height: 300px;"></div>--}}
                    {{--<canvas id="myChart" width="400" height="200"></canvas>--}}
                    <div class="chartjs-wrapper">
                        <canvas id="chartjs-dpr2" class="chartjs" width="undefined" height="100"></canvas>
                    </div>

                </div>
                <!-- /.card-body-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        Line Chart
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {{--<div id="line-chart" style="height: 300px;"></div>--}}
                    {{--<canvas id="myChart" width="400" height="200"></canvas>--}}
                    <div class="chartjs-wrapper">
                        <canvas id="chartjs-roe" class="chartjs" width="undefined" height="100"></canvas>
                    </div>

                </div>
                <!-- /.card-body-->
            </div>
        </div>
        <div class="col-md-6">
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        Line Chart
                    </h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {{--<div id="line-chart" style="height: 300px;"></div>--}}
                    {{--<canvas id="myChart" width="400" height="200"></canvas>--}}
                    <div class="chartjs-wrapper">
                        <canvas id="chartjs-dpr" class="chartjs" width="undefined" height="100"></canvas>
                    </div>

                </div>
                <!-- /.card-body-->
            </div>
        </div>
    </div>
    <!-- /.card -->
    <div class="row">
        <div class="col-md-12">
            <!-- The time line -->
            <div class="timeline">
                <!-- timeline time label -->
                <div class="time-label">
                    <span class="bg-red">24 Nov. 2019</span>
                </div>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <div>
                    <i class="fas fa-envelope bg-blue"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> 12:05</span>
                        <h3 class="timeline-header"><a href="#">We are preparing something cool here. Check out soon or
                                watch some kitty movies in the mean time</a></h3>

                        <div class="timeline-body">
                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                            quora plaxo ideeli hulu weebly balihoo...
                        </div>
                        <div class="timeline-footer">
                            <a class="btn btn-primary btn-sm">Read more</a>
                            <a class="btn btn-danger btn-sm">Delete</a>
                        </div>
                    </div>
                </div>
                <!-- END timeline item -->
                <!-- timeline item -->
                <div>
                    <i class="fas fa-user bg-green"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> 5 mins ago</span>
                        <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                        </h3>
                    </div>
                </div>
                <!-- END timeline item -->
                <!-- timeline item -->
                <div>
                    <i class="fas fa-comments bg-yellow"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> 27 mins ago</span>
                        <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
                        <div class="timeline-body">
                            Take me to your leader!
                            Switzerland is small and neutral!
                            We are more like Germany, ambitious and misunderstood!
                        </div>
                        <div class="timeline-footer">
                            <a class="btn btn-warning btn-sm">View comment</a>
                        </div>
                    </div>
                </div>
                <!-- END timeline item -->
                <!-- timeline time label -->
                <div class="time-label">
                    <span class="bg-green">3 Jan. 2014</span>
                </div>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <div>
                    <i class="fa fa-camera bg-purple"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> 2 days ago</span>
                        <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
                        <div class="timeline-body">
                            <img src="http://placehold.it/150x100" alt="...">
                            <img src="http://placehold.it/150x100" alt="...">
                            <img src="http://placehold.it/150x100" alt="...">
                            <img src="http://placehold.it/150x100" alt="...">
                            <img src="http://placehold.it/150x100" alt="...">
                        </div>
                    </div>
                </div>
                <!-- END timeline item -->
                <!-- timeline item -->
                <div>
                    <i class="fas fa-video bg-maroon"></i>

                    <div class="timeline-item">
                        <span class="time"><i class="fas fa-clock"></i> 5 days ago</span>

                        <h3 class="timeline-header"><a href="#">Mr. Doe</a> shared a video</h3>

                        <div class="timeline-body">
                            <div class="embed-responsive embed-responsive-16by9">

                            </div>
                        </div>
                        <div class="timeline-footer">
                            <a href="#" class="btn btn-sm bg-maroon">See comments</a>
                        </div>
                    </div>
                </div>
                <!-- END timeline item -->
                <div>
                    <i class="fas fa-clock bg-gray"></i>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    </div>
    <!-- /.timeline -->
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script>
        @foreach($charts as $chart)
        new Chart(
            document.getElementById("chartjs-{{$chart['id']}}"),
            {
                "type": "line",
                "data":
                    {
                        "labels": {!!  $chart['labels']!!},
                        "datasets":
                            [{
                                "label": "My First Dataset",
                                "data": {{$chart['data']}},
                                "fill": false,
                                "borderColor": "rgb(75, 192, 192)",
                                "lineTension": 0.1
                            },
                            {
                                "label": "",
                                "data": {{$chart['average']}},
                                "fill": false,
                                "borderColor": "rgb(175, 92, 192)",
                                "lineTension": 0.1
                            }]
                    }, "options": {}
            });
        @endforeach
        {{--new Chart(--}}
            {{--document.getElementById("chartjs-roe"),--}}
            {{--{--}}
                {{--"type": "line",--}}
                {{--"data":--}}
                    {{--{--}}
                        {{--"labels": {!! $labels !!},--}}
                        {{--"datasets": [{--}}
                            {{--"label": "My First Dataset",--}}
                            {{--"data": {!! $data !!},--}}
                            {{--"fill": false,--}}
                            {{--"borderColor": "rgb(75, 192, 192)",--}}
                            {{--"lineTension": 0.1--}}
                        {{--}]--}}
                    {{--}, --}}
                // "options": {
                //     legend: {
                //         display: false
                //     }}
            {{--});--}}
        {{--new Chart(--}}
            {{--document.getElementById("chartjs-dpr"),--}}
            {{--{--}}
                {{--"type": "line",--}}
                {{--"data":--}}
                    {{--{--}}
                        {{--"labels": {!! $labels !!},--}}
                        {{--"datasets": [--}}
                            {{--{--}}
                                {{--"label": "",--}}
                                {{--"data": {!! $data !!},--}}
                                {{--"fill": false,--}}
                                {{--"borderColor": "rgb(75, 192, 192)",--}}
                                {{--"lineTension": 0.1--}}
                            {{--},--}}
                            {{--{--}}
                                {{--"label": "",--}}
                                {{--"data": [38, 39, 40],--}}
                                {{--"fill": false,--}}
                                {{--"borderColor": "rgb(175, 92, 192)",--}}
                                {{--"lineTension": 0.1--}}
                            {{--}--}}
                        {{--]--}}
                    {{--},--}}
                {{--"options": {--}}
                    {{--legend: {--}}
                        {{--display: false--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}
        {{--new Chart(--}}
            {{--document.getElementById("chartjs-roe2"),--}}
            {{--{--}}
                {{--"type": "line",--}}
                {{--"data":--}}
                    {{--{--}}
                        {{--"labels": {!! $labels !!},--}}
                        {{--"datasets": [{--}}
                            {{--"label": "My First Dataset",--}}
                            {{--"data": {!! $data !!},--}}
                            {{--"fill": false,--}}
                            {{--"borderColor": "rgb(75, 192, 192)",--}}
                            {{--"lineTension": 0.1--}}
                        {{--}]--}}
                    {{--}, "options": {}--}}
            {{--});--}}
        {{--new Chart(--}}
            {{--document.getElementById("chartjs-dpr2"),--}}
            {{--{--}}
                {{--"type": "line",--}}
                {{--"data":--}}
                    {{--{--}}
                        {{--"labels": {!! $labels !!},--}}
                        {{--"datasets": [--}}
                            {{--{--}}
                                {{--"label": "",--}}
                                {{--"data": {!! $data !!},--}}
                                {{--"fill": false,--}}
                                {{--"borderColor": "rgb(75, 192, 192)",--}}
                                {{--"lineTension": 0.1--}}
                            {{--},--}}
                            {{--{--}}
                                {{--"label": "",--}}
                                {{--"data": [38, 39, 40],--}}
                                {{--"fill": false,--}}
                                {{--"borderColor": "rgb(175, 92, 192)",--}}
                                {{--"lineTension": 0.1--}}
                            {{--}--}}
                        {{--]--}}
                    {{--},--}}
                {{--"options": {--}}
                    {{--legend: {--}}
                        {{--display: false--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}

    </script>
@endsection('content')



