@extends('master.layout')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>Next Holiday in USA: {{ $comingHoliday->date }}</h3>

                    <p> {{ $comingHoliday->holiday }}</p>
                    <p> {{ $comingHoliday->description() }}</p>
                </div>
                <div class="icon">
                    <i class="fas fa-glass-cheers"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Upcoming dividends</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        @foreach($incomingDividends as $dividend)
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title"> {{ $dividend->ticker }}
                                        <span class="badge badge-success float-right">{{ $dividend->ex_date }}</span></a>
                                    <span class="product-description">
                              </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>

                </div>

                {{--<div class="card-body p-0">--}}
                    {{--<ul class="products-list product-list-in-card pl-2 pr-2">--}}
                        {{--@foreach($incomingDividends as $dividend)--}}
                            {{--<li class="item">--}}
                                {{--<div class="product-img">--}}
                                    {{--<img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">--}}
                                {{--</div>--}}
                                {{--<div class="product-info">--}}
                                    {{--<a href="javascript:void(0)" class="product-title"> {{ $dividend->ticker }}--}}
                                        {{--<span class="badge badge-success float-right">{{ $dividend->ex_date }}</span></a>--}}
                                    {{--<span class="product-description">--}}
                              {{--</span>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}

                {{--</div>--}}
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="javascript:void(0)" class="uppercase">Check all</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Upcoming earnings</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        @foreach($incomingEarnings as $earnings)
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">{{ $earnings->ticker }}
                                        <span class="badge badge-danger float-right">{{ $earnings->earnings_1 }}</span></a>
                                    <span class="product-description">
                                  </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="javascript:void(0)" class="uppercase">Check all</a>
                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection