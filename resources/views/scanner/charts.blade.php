@extends('master.layout')

@section('content')
    <div class="row {{isset($charts) ? '' : "justify-content-center"}}">
        <div class="col-md-4 justify-content-center">
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h2 class="card-title">
                        {{--<i class="far fa-chart-bar"></i>--}}
                        <strong>Select a company</strong>
                    </h2>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <!-- SEARCH FORM -->
                    <form class="form-inline ml-12 justify-content-center" method="GET">
                        <div class="input-group input-group-bg">
                            <input class="form-control form-control-navbar" type="search" placeholder="Enter company ticker" aria-label="Search" name="ticker">
                            <div class="input-group-append">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body-->
            </div>
        </div>

        <div class="col-md-4" {{isset($charts) ? "" : "hidden"}}>
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h2 class="card-title">
                        <strong>Important dates</strong>
                    </h2>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <p>Next Earnings:</p>
                    <p>Next Ex Dividend:</p>
                    <p>Next Pay Date:</p>
                </div>
                <!-- /.card-body-->
            </div>
        </div>

        <div class="col-md-4" {{isset($charts) ? "" : "hidden"}}>
            <!-- Line chart -->
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h2 class="card-title">
                        <strong>Basic metrics</strong>
                    </h2>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                    class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    Next Earnings: <br>
                    Next Ex Dividend: <br>
                    Next Pay Date: <br>
                </div>
                <!-- /.card-body-->
            </div>
        </div>
    </div>


    @if (isset($charts))
        @for($i = 0; $i < count($charts); $i ++)
            {{--@if ($i%2 === 0)--}}
                <div class="row">
            {{--@endif--}}
            <div class="col-md-12">
                <!-- Line chart -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="far fa-chart-bar"></i>
                            {{$charts[$i]['title']}}
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chartjs-wrapper">
                            <canvas id="chartjs-{{$charts[$i]['id']}}" class="chartjs" width="undefined" height="70"></canvas>
                        </div>

                    </div>
                    <!-- /.card-body-->
                </div>
            </div>

            {{--@if (($i-1)%2 === 0)--}}
                </div>
            {{--@endif--}}
        @endfor


    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script>
        @foreach($charts as $chart)
        new Chart(
            document.getElementById("chartjs-{{$chart['id']}}"),
            {
                "type": "line",
                "data":
                    {
                        "labels": {!!  $chart['labels']!!},
                        "datasets":
                            [{
                                "label": "{{$chart['id']}}",
                                "data": {{$chart['data']}},
                                "fill": false,
                                "borderColor": "rgb(75, 192, 192)",
                                "lineTension": 0.1,
                                ticks: {
                                    display: false,
                                }
                            },
                            {
                                "label": "Average",
                                "data": {{$chart['average']}},
                                "fill": true,
                                "borderColor": "lightgrey",
                                "lineTension": 0.1
                            }],

                    },
                    "options": {
                        legend: {
                            display: false
                        },
                        elements: {
                            point: {
                                radius: 2
                            }
                        },
                        // scales: {
                        //     yAxes: [{
                        //         ticks: {
                        //             display: false
                        //         }
                        //     }]
                        // }
                }
            });
        @endforeach
        </script>
    @endif
@endsection('content')



