<div class="card card-default">
    <div class="card-header">
        <h3 class="card-title">Filters</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form role="form" id="filters"action="/submit">
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">Years</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "NoYrs", "selected" => ">="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="NoYrs" id="NoYrs" value="10">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">Dividend Yield [%]</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "yield", "selected" => ">="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="yield" id="yield" value="2.0">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">Dividend Payout Ratio [%]</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "DPR", "selected" => "<="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="DPR" id="DPR" value="60">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">PE</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "pe", "selected" => "<="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="pe" id="pe" value="20">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">ROE</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "roe", "selected" => ">="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="roe" id="roe" value="10">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">Market Capitalization [mil $]</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "mktcap", "selected" => ">="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="mktcap" id="mktcap" value="10000">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label style="align-content: center">Debt / Equity</label>
                        <div class="row">
                            <div class="col-md-4">
                                @include('scanner.partials.select2', ["name" => "debtequity", "selected" => "<="])
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="debtequity" id="debtequity" value="1">
                            </div>
                        </div>
                    </div>
                </div>

                {{--<div class="col-md-3">--}}
                    {{--<div class="form-group">--}}
                        {{--<label style="align-content: center">Debt / Equity</label>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--@include('scanner.partials.select2', ["name" => "industry", "selected" => "<="])--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                                {{--<select class="form-control select2bs4" style="width: 100%;" name="{{ "select2" . $name }}">--}}
                                    {{--<option selected="selected"> {{ $selected }}</option>--}}
                                    {{--{{dd($values)}}--}}
                                    {{--@foreach($values as $value)--}}
                                        {{--<option>{{ $value }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                {{--<input type="text" class="form-control" name="debtequity" id="debtequity" value="1">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <button type="submit" class="btn btn-primary">Filter</button>
            <button class="btn btn-primary" id="restore">Restore defaults</button>
            <button class="btn btn-primary" id="offAll">Turn off all filters</button>
        </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Last update {{ $updatedAt }}
    </div>
</div>
<!-- /.card -->