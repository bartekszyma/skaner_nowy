@extends('master.layout')

@section('style')
    <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="   crossorigin="anonymous"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

    @if($demo)
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>Wersja demo</h3>

                <p style="margin-left: 20px">Dane na dzień {{config('demo.scanner.demo_date')}}</p>
                <p style="margin-left: 20px">Wyświetlane wpisy ograniczone do 100</p>
            </div>
            <div class="icon">
                <i class="fas fa-bullhorn"></i>
            </div>
        </div>
    @endif

    @include('scanner.filters')

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Stocks fulfilling criteria</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {{--<table id="example1" class="table table-bordered table-striped">--}}
                <table id="y_daily" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--<th>Data</th>--}}
                        <th>Ticker</th>
                        <th>Name</th>
                        <th>Sector</th>
                        <th>Industry</th>
                        <th>Yrs</th>
                        <th>DY</th>
                        <th>DPR</th>
                        <th>PE</th>
                        <th>ROE</th>
                        <th>MktCap</th>
                        <th>D/E</th>
                        <th>Beta</th>
                        <th>Close</th>
                        {{--<th>Wyniki</th>--}}
                        {{--<th>Wyniki [do]</th>--}}
                        {{--<th>Ex Dividend</th>--}}
                        {{--<th>24h max</th>--}}
                        {{--<th>24h min</th>--}}
                        {{--<th>1yr max</th>--}}
                        {{--<th>1yr min</th>--}}
                        {{--<th>DY</th>--}}
                    </tr>
                    </thead>

                </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('scripts')
<script>

    $(document).ready( function () {
        var x = $('#y_daily').DataTable({
            lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
            // columnsDef: [
            //     { "width": "10px", "targets": 0 },
            //     { "width": "35%", "targets": 1 },
            //     { "width": "10%", "targets": 3 },
            //     ],
            // columns: [
            //     { "width" : "5px"},
            // ],
            ajax: {
                url: '{{url('/yahoo_daily')}}',
                dataSrc: ''
            },
            columns: [
                // { "data": "date" },
                { "data": "ticker" },
                { "data": "CompanyName" },
                { "data": "Sector"},
                { "data": "Industry"},
                { "data": "NoYrs"},
                { "data": "yield"},
                { "data": "DPR"},
                { "data": "pe" },
                { "data": "ROE"},
                { "data": "mktcap" },
                { "data": "DebtEquity"},
                { "data": "beta" },
                { "data": "close" },
                // { "data": "earnings_1"},
                // { "data": "earnings_2" },
                // { "data": "ex_date" },
                // { "data": "day_max" },
                // { "data": "day_min" },
                // { "data": "year_max" },
                // { "data": "year_min"},
                // { "data": "forward_dividend"},
            ]
        });
        //deals with resizing
        x.ajax.url("{{route('scanner-filter-results')}}").load()
        // x.fnAdjustColumnSizing();// https://legacy.datatables.net/ref#fnAdjustColumnSizing
        // setTimeout( x.ajax.reload, 5000)

        document.getElementById('filters').addEventListener('submit', (event) => {
            event.preventDefault()
            var formElements = document.getElementById("filters").elements
            var get = ''
            for (i = 0; i < formElements.length - 1; i++) {
                get += formElements[i].name + '=' + formElements[i].value + '&'
            }
            get = get.substring(0, get.length - 1)
            x.ajax.url("{{route('scanner-filter-results') . '/?'}}" + get).load()

        })

        $("#restore").click(function() {
            $('#filters').trigger('reset');
        })
        $("#offAll").click(function() {
            $('form#filters :input').each(function() {
                $(this).val('off');
            });
        })

    } );
</script>

<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endsection
