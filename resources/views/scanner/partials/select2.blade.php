@php
$values = ($selected == '>=') ? ['>', '=', '<=', '<', 'off'] : ['>', '>=', '=', '<', 'off'];
@endphp

<select class="form-control select2bs4" style="width: 100%;" name="{{ "select2" . $name }}">
    <option selected="selected"> {{ $selected }}</option>
    {{--{{dd($values)}}--}}
    @foreach($values as $value)
        <option>{{ $value }}</option>
    @endforeach
</select>