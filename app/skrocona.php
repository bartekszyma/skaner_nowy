<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class skrocona extends Model
{
    protected $table = 'skrocona';

    public static function latestDate()
    {
        return self::orderBy('Date', 'Desc')->first()->Date;
    }

    public function getROEAttribute($value)
    {
        return number_format($value, 2);
    }

    public function getFormattedRoeAttribute()
    {
        return number_format($this->attributes['ROE'], 2);
    }

    public function setROEAttribute($value)
    {
        $this->attributes['ROE'] = number_format($value, 2);
    }

    public function getHistoricalStockData(string $stock, string $param)
    {
        return $this->where('ticker', '=', $stock)->get([$param, 'Date']);
    }
}
