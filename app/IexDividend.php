<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IexDividend extends Model
{
    protected $guarded = [];
    protected $table = 'iex_dividends_db';
    const UPDATED_AT = null;
    const CREATED_AT = null;

}
