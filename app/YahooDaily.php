<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class YahooDaily extends Model
{
    const DATA_FORMAT = 'Y-m-d';

    protected $table = 'yahoo_daily';

    public static function latestDate()
    {
        return self::orderBy('date', 'DESC')->first()->date;
    }

    public function getStocksWithExDdOlderThen3Months()
    {
        $lastDate = Carbon::parse('-3 months');

        return $this->where
        ([
            ['date', '=', self::latestDate()],
            ['ex_date', '<', $lastDate]
        ])
            ->get();
    }

    public function filter($filters, $having, $updatedAt)
    {
        $having = ($having == '') ? '1' : $having;

        return DB::table('yahoo_daily')
            ->select(DB::raw(
                'yahoo_daily.*,
                skrocona.*,
                FORMAT(skrocona.ROE, 2) as ROE,
                FORMAT((yahoo_daily.forward_dividend / yahoo_daily.eps_ttm) * 100, 2) as DPR'
            ))
            ->join('skrocona', 'yahoo_daily.ticker', '=', 'skrocona.ticker')
            ->where('yahoo_daily.date', '=', $updatedAt)
            ->where('skrocona.Date', '=', skrocona::latestDate())
            ->where($filters)
            ->havingRaw($having);
    }

    public function getIncomingDividends($startDate, $endDate)
    {
        return $this->where('date', '=', self::latestDate())
            ->whereBetween('ex_date', [$startDate->format(self::DATA_FORMAT), $endDate->format(self::DATA_FORMAT)])
            ->orderBy('ex_date')
            ->get();
    }

    public function getIncomingEarnings($startDate, $endDate)
    {
        return $this->where('date', '=', self::latestDate())
            ->whereBetween('earnings_1', [$startDate->format(self::DATA_FORMAT), $endDate->format(self::DATA_FORMAT)])
            ->orderBy('ex_date')
            ->get();
    }

    public function getAllFromLatestUpdate($ticker, $operator = '>')
    {
        return $this->where([['date', '=', self::latestDate()], ['ticker', $operator, $ticker]])->get();
    }


}
