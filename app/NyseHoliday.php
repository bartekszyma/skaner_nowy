<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class NyseHoliday extends Model
{
    const UPDATED_AT = null;
    const CREATED_AT = null;

    public function getComing()
    {
        $today = new Carbon('today');
        return $this->where('date', '>', $today->format('Y-m-d'))->first();
    }

    public function holidayDisclaimer()
    {
        return $this->hasOne('App\HolidayDisclaimer', 'id', 'disclaimer');
    }
    public function description()
    {
        return HolidayDisclaimer::find($this->disclaimer)->description;
    }
}
