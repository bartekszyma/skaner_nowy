<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wizyty extends Model
{
    protected $table = 'wizyty';
    const UPDATED_AT = null;
    const CREATED_AT = null;
}
