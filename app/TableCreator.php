<?php

namespace App;


class TableCreator
{
    public static function createCalendar($portfolio)
    {
        $calendarArray = [];

        $portfolio->each(function ($stock) use (&$calendarArray) {
            $monthE = (int)date("m", strtotime($stock->exDate));
            $monthP = (int)date("m", strtotime($stock->paymentDate));
            $calendarArray[$stock->symbol] [$monthE] .= " Ex";
            $calendarArray[$stock->symbol] [$monthP] .= " P";
        });

        $months = [
            1 => "I",
            2 => "II",
            3 => "III",
            4 => "IV",
            5 => "V",
            6 => "VI",
            7 => "VII",
            8 => "VIII",
            9 => "IX",
            10 => "X",
            11 => "XI",
            12 => "XII"
        ];

        return self::createTable($months, $calendarArray);
    }

    public static function createTable($headers, $body, $tableClass = 'table table-fixed table-bordered table-striped')
    {
        $table = "<table class=$tableClass><thead><tr><th>Ticker</th>";

        foreach ($headers as $header) {
            $table .= "<th>" . $header . "</th>";
        }
        $table .= "</tr></thead><tbody>";

        foreach ($body as $k => $v) {
            $table .= "<tr><td>$k</td>";

            for ($i = 1; $i <= 12; $i++) {
                $table .= ($body[$k][$i] !== null) ? "<td style = 'text-align: center'>$v[$i]</td>" : "<td></td>";
            }
            $table .= "</tr>";
        }

        return $table .= "</tbody></table>";
    }
}