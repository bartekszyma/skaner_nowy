<?php

namespace App\Http\Controllers;

use App\skrocona;
use App\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(Request $request)
    {
//        Log::info('test t=log');
        return dd($request);
    }

    public function IexTest()
    {
//        $iex = new IexScrapper();
//        return $iex->getDividendData('MSFT', 'ytd');

        $s = new skrocona();
        $roeData = $s->getHistoricalStockData('MSFT', 'ROE');
        $labels = '[';
        $data = '[';

        foreach ($roeData as $roe) {
            $labels .= "'" . $roe->Date . "'" . ", ";
            $data .= "$roe->ROE, ";
        }
        $labels = rtrim($labels, ', ');
        $data = rtrim($data, ', ');
        $labels .= ']';
        $data .= ']';
        $charts = [
            0 => [
                'title' => 'ROE',
                'labels' => $labels,
                'data' => $data,
                'id' => 'ROE',
                'average' => '[39, 38, 37]',
            ]
        ];

        return view('growing-dividends.timeline', compact('charts', 'labels', 'data'));
    }

    public function toggleDemo()
    {
//        $user = new User((array)Auth()->user());
        $u = Auth()->user();
        $user = new User();
        $user = $user->where('id', '=', $u->id)->get()->first();
        $scanner = ($user->scanner == 0) ? 1 : 0;
//        dd($user);
//        $user->id = $u->id;
        $user->scanner = $scanner;
        $user->save();
//        $user->update(['scanner', $scanner]);
//dd($user);
        return redirect('/scanner/filter');
    }
}
