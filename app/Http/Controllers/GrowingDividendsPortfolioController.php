<?php

namespace App\Http\Controllers;

use App\Portfolio;
use App\TableCreator;

error_reporting(2);

class GrowingDividendsPortfolioController extends Controller
{
    const GROWING_PORTFOLIO_USER_ID = 0;

    public function index()
    {
        $start = '2017-01-31';
        $end = '2017-12-31';
        $portfolio = new Portfolio();
        $portfolio = $portfolio->addDividendDatesToPortfolio(self::GROWING_PORTFOLIO_USER_ID, $start, $end);
        $table = TableCreator::createCalendar($portfolio);

        return view('growing-dividends.assets', compact('portfolio', 'table'));
    }

    public function timeline()
    {
        $portfolio = new Portfolio();

        $portfolio = $portfolio->addDividendDatesToPortfolio(self::GROWING_PORTFOLIO_USER_ID);
//        dd($portfolio);

        return view('growing-dividends.timeline');
    }

}
