<?php


namespace App\Http\Controllers;



use App\GuruFocus;
use App\Scrappers\GuruFocusScrapper;
use Carbon\Carbon;

class GuruFocusController extends Controller
{

    public function getFinancials($ticker)
    {
        $newest = GuruFocus::where('ticker', '=', $ticker)->first();
        $last_quarter = is_null($newest) ? new Carbon('1900-01-01') : new Carbon($newest->last_quarter);
        $today = new Carbon('today');

        if ($last_quarter->diff($today)->days > 105) {
            $scrapper = new GuruFocusScrapper();
            $scrapper->scrap($ticker, 'scrap_financials');
        }
    }
}