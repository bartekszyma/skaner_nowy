<?php

namespace App\Http\Controllers\Accountant;

use App\FileProcessor;
use App\FxData;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\ListOfEvents;
use App\StockEvent;
use App\FileProcessor as FP;
use Illuminate\View\View;
use File;
use Response;
use Carbon\Carbon;

//todo auth
//todo code sniffer or something for static analysis
class ProcessFileController extends Controller
{
    public function process(Request $request)
    {
        abort_unless(auth()->user()->permission, 403);
//        $time = Carbon::now();
//        abort_unless(auth()->user()->expires > $time, 403);

        error_reporting(1);
        $daterange = explode(':', $request->get('daterange'));
        $date_from = str_replace([" ", "-"], ["", ""], $daterange[0]);
        $date_to = str_replace([" ", "-"], ["", ""], $daterange[1]);
        $output = [];
        $output["all"] = ["STK" => [], "DIV" => [], "OPT" => []];
        if ($request->file('akcje') !== null) {
            (new FP($request->file('akcje')->getRealPath()))->createEvents();
        }
        if (($request->file('cash') !== null)){
            (new FP($request->file('cash')->getRealPath()))->createEvents();
        }

        $output = [];
        $output["sums"] = ListOfEvents::calculateSumsForInstantEvents($date_from, $date_to);
        $output["all"] = (ListOfEvents::getListOfEvents(null, $date_from, $date_to));
        $output["history"] = ListOfEvents::createStocksHistory();
//        $output["Stocks"] = $output["all"]["STK"];

//        $output["events"] = collect(ListOfEvents::getStocks())->groupBy('CurrencyPrimary')->sum('ProfitInThisTransactionInFx');

        $output["STK"] = ListOfEvents::getListOfAssetTypeGroupedByCurrencyAndSymbol('STK', $date_from, $date_to);

        foreach($output["STK"] as $key => $currency) {
            foreach($currency as $stock_group){
                $output["sums_fx"][$key] += collect($stock_group)->sum('ProfitInThisTransactionInFx');
                $output["stocks_pln"] += collect($stock_group)->sum('ProfitInThisTransactionInBase');
            }
        }
        $output["Fx"] = FxData::getInstance()->getFxRates();

        $output["mostRecentFxRates"] = $output["Fx"]->last()->toArray();
//        $output["TimeRange"] = ListOfEvents::getListOfEventsInTimeRange(20180101, 20180631);
//        $output["Div"] = $output["TimeRange"]["DIV"];

//        dd($output["all"]);
        $request->session()->put('output', $output);
        return view()->make("dashboard", compact("output"));
    }

    public function load(Request $request)
    {
        error_reporting(1);
        FxData::getInstance();
        return view('accountant.load_files')->with('request', $request);
    }

    public function process3(Request $request)
    {
        if($request->file('akcje') == null) {

        }
        $stocks = file_get_contents($request->file('akcje')->getRealPath());
        $request->session()->put('stocks', $stocks);
        return view('load_files')->with('request', $request);
    }

    public function downloadJSONFile(Request $request)
    {
        $data = json_encode($request->session()->get('output')["all"]);
        $fileName = time() . '_datafile.json';
        File::put(public_path('/upload/json/'.$fileName),$data);
        return Response::download(public_path('/upload/json/'.$fileName))->deleteFileAfterSend(true);
    }

    public function merge(Request $request)
    {
//        error_reporting(1);
//        if ($request->method() === 'POST') {
//            $contents = '';
//            foreach($request->files->all() as $files) {
//                foreach ($files as $file) {
//                    $contents .= file_get_contents($file->getRealPath());
//                }
//            }
//
//        }
//        $fileName = time() . '_datafile.csv';
//        File::put(public_path('/upload/merged/' . $fileName), $contents);

        return view('merge')->with('request');
    }
    public function downloadMerged(Request $request)
    {
        error_reporting(1);
        $contents = '';
        $files_merged_as_array_keyd_by_transactionID = [];
        $non_unique_values = [];
        if ($request->method() === 'POST') {

            foreach($request->files->all() as $files) {
                foreach ($files as $file) {
                    $fp = new FileProcessor($file->getRealPath);
                    dd($fp->cleanInputAndCreateAssociativeCollection());
                    $contents .= file_get_contents($file->getRealPath());
                }
            }
        }
        $contents_array = explode("\n", $contents);
//        $headers = $contents_array[0];
//        if (! in_array("TransactionID")){
//            dd("TransactionID not in file, load file with TransactionID");
//        }
        $contents_array = collect($contents_array)
            ->unique()
            ->map(function ($line) {
                return $line.PHP_EOL;
            })
            ->toArray();

        $fileName = time() . '_datafile.csv';
        File::put(public_path('/upload/merged/' . $fileName), $contents_array);
        return Response::download(public_path('/upload/merged/' . $fileName))->deleteFileAfterSend(true);
    }

    public function saveCsv(Request  $request)
    {
        $fileName = time() . '_datafile.csv';
//        dd($request);
        File::put(public_path('/upload/csv/' . $fileName), $request->input('csv'));
        return Response::download(public_path('/upload/csv/' . $fileName))->deleteFileAfterSend(true);
    }
}
