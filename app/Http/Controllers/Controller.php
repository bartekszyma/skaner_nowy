<?php

namespace App\Http\Controllers;

use App\Wizyty;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
    }

    public function callAction($method, $arguments) {
        if(method_exists($this, $method)) {
            $this->registerVisit();
            return call_user_func_array(array($this,$method),$arguments);
        }
    }

    protected function registerVisit()
    {
        if (null != Auth::user())
        {
            $uri = url()->current();
            $wizyta = new Wizyty();
            $wizyta->User = Auth::user()->email;
            $wizyta->Strona = $uri;
            $wizyta->save();
        }
    }
}
