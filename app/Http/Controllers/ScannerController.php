<?php

namespace App\Http\Controllers;

use App\NyseHoliday;
use App\skrocona;
use App\YahooDaily;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ScannerController extends Controller
{
    const skroconaColumns = ['noyrs', 'roe', 'debtequity', 'companyname'];
    const calculatedColumns = ['dpr', 'roe'];

    private $updateAt;

    public function __construct()
    {
        parent::__construct();
        /*
         * middleware runs before constructor so it is not possible to
         * use auth in constructor
         *
         * $this->updateAt = ($this->demo()) ? config('demo.scanner.demo_date') : YahooDaily::latestDate();
         */
    }

    public function index()
    {
        $nyseHoliday = new NyseHoliday();
        $comingHoliday = $nyseHoliday->getComing();
        $startDate = new Carbon('today');
        $endDate = $startDate->copy()->addDays(7);
        $yd = new YahooDaily();

        $incomingDividends = $yd->getIncomingDividends($startDate, $endDate);
        $incomingEarnings = $yd->getIncomingEarnings($startDate, $endDate);
        return view('scanner.dashboard', compact('comingHoliday', 'incomingDividends', 'incomingEarnings'));
    }

    public function filter()
    {
        $demo = $this->demo();
        $updatedAt = ($this->demo()) ? config('demo.scanner.demo_date') : YahooDaily::latestDate();

        return view('scanner.data', compact('updatedAt', 'demo'));
    }

    public function getFilteredData(Request $request)
    {
        $yd = new YahooDaily();
        $conditions = $this->getFiltersFromRequest($request);
        $updatedAt = ($this->demo()) ? config('demo.scanner.demo_date') : YahooDaily::latestDate();

        $data = $yd->filter($conditions['where'], $conditions['having'], $updatedAt);

        if ($this->demo()) {
            $limit = config('demo.scanner.limit_visible_rows');
            $data->limit($limit);
        }

        return $data->get();
    }

    private function getFiltersFromRequest(Request $request)
    {
        $filters = $request->all();
        $filterNames = array_keys($filters);
        $filterValues = array_values($filters);
        $having = '';
        $where = [];
        for ($i = 0; $i < count($filters) - 1; $i += 2) {
            $column = $filterNames[$i + 1];
            $table = 'yahoo_daily.';
            $operator = $filterValues[$i];
            $value = $filterValues[$i + 1];

            if ($operator == 'off') {
                continue;
            }

            if (in_array(strtolower($column), self::calculatedColumns)) {
                $having .= " $column $operator $value and";
                continue;
            } else if (in_array(strtolower($column), self::skroconaColumns)) {
                $table = 'skrocona.';
            }

            $where[] = [$table . $column, $operator, $value];
        }
        return ['where' => $where, 'having' => rtrim($having, ' and')];
    }

    private function demo()
    {
        if (Auth()->user()->scanner == 0) {
            return true;
        }

        return false;
    }

    public function charts(Request $request)
    {
        $ticker = $request->get('ticker');
        if ($ticker == null) {
            return view('scanner.charts');
        }
        $s = new skrocona();

        $data = $s->where('ticker', '=', $ticker)->get();
        $collections = [
            'roe' => $data->pluck('ROE'),
            'divYield' => $data->pluck('DivYield'),
            'dpr' => $data->pluck('EPSPayout'),
            'Debt/Equity' => $data->pluck('DebtEquity'),
            'pe' => $data->pluck('PE'),
        ];
        $date = $data->pluck('Date');
        $date = "['" . $date->implode("', '") . "']";

        $charts = [];
        foreach ($collections as $name => $collection) {
            $avg = number_format($collection->avg(), 2, '.', '');
            $values = '[' . $collection->map(function ($item) {return number_format($item, 2);})->implode(', ') . ']';
            $average = "[" . Collection::times($collection->count(), function ($number) use ($avg) {return $avg;})->implode(", ") . "]";

            $charts[] = [
                'title' => strtoupper($name),
                'labels' => $date,
                'data' => $values,
                'average' => $average,
                'id' => $name,
            ];
        }

        return view('scanner.charts', compact('charts'));
    }
}
