<?php

namespace App\Http\Controllers;


use App\EmailToken;
use App\ExcelWorkshopsData;
use App\ExcelWorkshopsDataRequest;
use App\GuruFocus;
use App\Scrappers\GuruFocusScrapper;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function index(Request $request, string $ticker)
    {
        $url = $request->fullUrl();
        $token = $request->get('token');
        /*
         * forces deleting existing entries from excel_workshops_data table and recalculates them based on guru_focus table
         */
        $forced = $request->get('forced') ?? false;
/*
 * todo remove
 */
/* => */$forced = true;

        $et = (new EmailToken())->where('token', $token)->get();

        $ewdr = new ExcelWorkshopsDataRequest();
        $ewdr->log($token, $ticker, $url);

        if ($et->count() === 0) {
            return new JsonResponse( 'Invalid token', 401);
        }

        try {
            $scrapper = new GuruFocusScrapper();
            $wasScrapped = $scrapper->scrap($ticker, 'scrap_financials');
            $financials = GuruFocus::where([['ticker', '=', $ticker], ['type', '=', 'financials']])->orderBy('last_quarter', 'DESC')->first();
            $estimates = GuruFocus::where([['ticker', '=', $ticker], ['type', '=', 'estimates']])->orderBy('last_quarter', 'DESC')->first();

            if ($financials === null) {
                return new JsonResponse('Ups, not found ' . strtoupper($ticker) . '. Are you sure it\'s not Yeti?');
            }

            if (! is_null($estimates)) {
                $estimates = json_decode($estimates->data, true);
                $estimates = $estimates['annual'];
            }

            $financialsData = json_decode($financials->data, true);
            $financialsData = $financialsData['financials'];

            if ($wasScrapped || $forced) {
                ExcelWorkshopsData::where('ticker', '=', $ticker)->delete();
                (new ExcelWorkshopsData())->fromArray($financialsData, $estimates, $ticker, 'annuals');
                (new ExcelWorkshopsData())->fromArray($financialsData, $estimates, $ticker, 'quarterly');
            }

        } catch (\Throwable $t) {
            $ewdr->errors = json_encode([$t->getMessage() => $t->getTrace()]);
            $ewdr->update();
            return new JsonResponse( 'Oops ... something\'s wrong :/ Write us a message with error number: ' . $ewdr->id, 500);
    }
        return $this->showData($ticker, $request->region, $request);
    }

    private function showData(string $ticker, $region)
    {
        $guruData = new ExcelWorkshopsData();
        $annuals = $guruData->getXYears($ticker, 6);
        $quarters = $guruData->getXQuarters($ticker);

        $data = array_merge($annuals, $quarters);
        $last_quarter = new Carbon (GuruFocus::where('ticker', '=', $ticker)->orderBy('last_quarter')->first()->last_quarter);

        $number_format = [
            "decimal" => $region === "pl" ? "," : ".",
            "thousands" => $region === "pl" ? "" : " "
        ];
        /*
         * index 0 if always for TTM
         * index 1 is for most recent data
         *
         *
         * $mostRecent = new Carbon($data[1]['fy'] . '-01');
         *
         *
         * there might not be 6 years of data for given company
         * in that case return all data
         */
//        if (count($data) >= 6) {
//
//            if ($mostRecent->diff($last_quarter)->days > 180) {
//                unset($data[count($data) - 1]);
//            } else {
//                unset($data[1]);
//            }
//        }

        return view('data.endpoint', compact('data','annuals', 'quarters', 'ticker', 'number_format'));
    }
}