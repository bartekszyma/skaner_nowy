<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GwentController extends Controller
{
    public function index()
    {
        return view('gwent');
    }

    public function sql(Request $request)
    {
        $txt = $request->get('txt');
        $records = explode("\n", $txt);
        $sqlQueries = "insert into external_products
                 (
                     internal_id,
                     external_id,
                     platform_agnostic_id, 
                     platform_id, 
                     product_type, 
                     title, 
                     description, 
                     status, 
                     created_at, 
                     updated_at
                 )
            values 
            ";

        foreach ($records as $record)
        {
            $firstComma = strpos($record, ',');
            $id = substr($record, 0, $firstComma);
            $title = substr($record, $firstComma + 1);
            $platform = (strpos($title,'GAEA')) ? 8 : 0;

            $sqlQueries .= $this->generateValues($id, $title, $platform);
        }
        return ($sqlQueries);
        return view('gwent', compact($sqlQueries));
    }

    private function getIdAndTitle($record)
    {
        $firstComma = strpos($record, ',');
        $id = substr($record, 0, $firstComma);
        $title = substr($record, $firstComma);

        return [$id, $title];
    }

    private function generateValues($id, $title, $platform_id)
    {
        $prefix = 'inp_';
        $product_type = 'inapp';
        $description = 'manually added';
        $status = 'active';
        $created_at = 'now()';
        $updated_at = $created_at;

        $internal_id = $prefix . $id;
        $external_id = $id;
        $platform_agnostic_id = $id;

        return "
                (
                    '$internal_id',
                    $external_id,
                    $platform_agnostic_id,
                    $platform_id,
                    '$product_type',
                    '$title',
                    '$description',
                    '$status',
                    $created_at,
                    $updated_at
                ) ";
    }
}
