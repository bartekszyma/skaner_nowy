<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    public function getUserPortfolio($userId)
    {
        return $this->where('userId', '=', $userId)
            ->orderBy('portfolios.symbol')
            ->get();
    }

    public function addDividendDatesToPortfolio($userId, $start = null, $end = null)
    {
        /*
         * todo when enough data in iex_dividends_db
         * $date = Carbon::parse('last year');
         * $exStart = $date->copy()->startOfYear();
         * $edEnd = $date->copy()->endOfYear();
         */

        if ($start == null or $end == null) {
            return $this->where('userId', '=', $userId)
                ->distinct('symbol')
                ->join('iex_dividends_db', 'iex_dividends_db.Symbol', '=', 'portfolios.symbol')
                ->where('iex_dividends_db.exDate', '>=', $this->getPortfolioFirstDate($userId))
                ->orderBy('portfolios.symbol')
                ->get();
        }

        return $this->where('userId', '=', $userId)
            ->distinct('symbol')
            ->join('iex_dividends_db', 'iex_dividends_db.Symbol', '=', 'portfolios.symbol')
            ->whereBetween('iex_dividends_db.exDate', [$start, $end])
            ->orderBy('portfolios.symbol')
            ->get();
    }

    public function getPortfolioFirstDate($userId)
    {
        return $this->where('userId', '=', $userId)
        ->orderBy('portfolios.date')
        ->first()
        ->date;
    }

}
