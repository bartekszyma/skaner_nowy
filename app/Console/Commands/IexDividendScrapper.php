<?php

namespace App\Console\Commands;

use App\Api\IexScrapper;
use App\ErrorLog;
use App\IexDividend;
use App\YahooDaily;
use Illuminate\Console\Command;

class IexDividendScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "iex_scrap:dividends {range} {stock=''} {operatorCode=2}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get dividend dates from API';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $operatorCodes = [0 => '<', 1 => '=', 2 => '>'];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stock = $this->argument('stock');
        $operator = $this->operatorCodes[$this->argument('operatorCode')];

        $iex = new IexScrapper();
        $yahoo = new YahooDaily();
        $noData = [];
        $exceptions = [];
        $stocksWithNoExDd = $yahoo->getAllFromLatestUpdate($stock, $operator);

        $iex->getData('dividends', []);
        die('success');
        $numberOfStocks = count($stocksWithNoExDd);
        $counter = 0;

        foreach ($stocksWithNoExDd as $stock) {
            $ticker = $stock->ticker;
            $counter += 1;
            try {
                $jsonData = json_decode($iex->getDividendData($stock->ticker, $this->argument('range')));

                if (!isset($jsonData[0])) {
                    $noData[] = $ticker;
                    continue;
                }
                $dividendData = new IexDividend((array)$jsonData[0]);
                var_dump($dividendData);
                $dividendData->symbol = $ticker;
                if (!IexDividend::where([['symbol', '=', $ticker], ['exDate', '=', $dividendData->exDate]])->get()->isEmpty()) {
                    continue;
                }
                $dividendData->save();
            } catch (\Exception $e) {
                $exceptions[$ticker] = $e->getMessage();
                continue;
            }
        }
        if ($counter == $numberOfStocks) {
            print_r("Success");
        }
        print_r($noData);
        print_r($exceptions);

        $this->log($exceptions);
    }

    public function log($logs)
    {
        foreach ($logs as $key => $value) {
            $error = new ErrorLog();
            $error->description = $key . ' ' . $value;
        }
    }
}
