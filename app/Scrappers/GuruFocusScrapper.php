<?php


namespace App\Scrappers;

use App\GuruFocus;
use App\APIClients\GuruFocusApiClient;
use Carbon\Carbon;

class GuruFocusScrapper
{
    const CONFIG = 'scrappers.GuruFocus';
    private $apiClient;
    private $scenarios;

    public function __construct()
    {
        $this->apiClient = new GuruFocusApiClient();
        $this->scenarios = config(self::CONFIG . '.scenarios');
    }

    public function scrap(string $ticker, string $scenario): bool
    {
        $newest = GuruFocus::where('ticker', '=', $ticker)->first();
        $last_quarter = is_null($newest) ? new Carbon('1900-01-01') : new Carbon($newest->last_quarter);
        $last_insert = is_null($newest) ? new Carbon('1900-01-01') : new Carbon($newest->created_at);
        $today = new Carbon('today');

//        if ($last_insert->diff($today)->days < 3) { //|| $last_quarter->diff($today)->days < 90) {
//            return false;
//        }

        foreach ($this->scenarios[$scenario] as $endpoint) {
            $data = $this->apiClient->getData($ticker, $endpoint);
            if (empty($data)) {
                return false;
            }
            $json = json_decode($data, true);

            if ($endpoint === 'financials')
            {
                $quarters = $json['financials']['quarterly']['Fiscal Year'];
                $lastQuarter = new Carbon($quarters[count($quarters) - 1]);
            } else {
                $quarters = empty($json['quarter']['date']) ? $json['annual']['date'] : $json['quarter']['date'];
                $lastQuarter = new Carbon($quarters[count($quarters) - 1]);
            }
            $this->save($ticker, $endpoint, $data, $lastQuarter);
        }
        return true;
    }

    private function save(string $ticker, string $type, string $data, string $lastQuarter)
    {
        $lastRecord = GuruFocus::where([['ticker', '=', $ticker], ['last_quarter', '=', $lastQuarter]])
            ->orderBy('last_quarter')
            ->get();
        if ($lastRecord->count() === 0) {
            $gf = new GuruFocus();

            $gf->ticker = $ticker;
            $gf->last_quarter = $lastQuarter;
            $gf->data = $data;
            $gf->type = $type;
            $gf->save();
        }
    }

}