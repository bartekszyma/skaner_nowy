<?php
if (! function_exists('uriEnd')) {
    function uriEnd($uri)
    {
        $route = explode("/",$uri);

        return $route[count($route) - 1];
    }
}