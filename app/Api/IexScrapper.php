<?php

namespace App\Api;


use App\IexCost;
use Carbon\Carbon;

class IexScrapper
{
    /* OLD IMPLEMENTATION remove after testing
    const API_VERSION = 'V1';
    const API_BASE_URI = 'https://cloud.iexapis.com/';

    private $cost = 0;

    const DIVIDEND_RANGES =
        [
            '5y',  // Five years Historical market data
            '2y',  // Two years Historical market data
            '1y',  // One year Historical market data
            'ytd', // Year-to-date Historical market data
            '6m',  // Six months Historical market data
            '3m',  // Three months Historical market data
            '1m',  // One month (default) Historical market data
            'next' // The next upcoming dividend
        ];
    */

    private $config;

    public function __construct()
    {
        $this->config = $this->getConfig();
    }

    public function getData($endpointName, $inputParams = [])
    {
        $endpointName = strtoupper($endpointName);
        $endpointConfig = $this->config['ENDPOINTS'][$endpointName];
        $path = $endpointConfig['URI']['PATH'] . '/';
        $args = $endpointConfig['URI']['ARGS'];

        $pathAsArray = explode('/', $path);
        $requiredArgs = $this->getRequiredParams($pathAsArray, $args);
        /*
         * todo add default args value if arg not provided
         */
        if ($this->checkIfAllParamsAreProvided($requiredArgs, $inputParams)) {
            $inputParams = $this->validateParams($inputParams, $endpointConfig['URI']['ALLOWED_ARGS_VALUES']);
        }
        dd($this->createEndpointUri($pathAsArray, $args, $inputParams));
    }

    private function createBaseUri()
    {
        return $baseUri = $this->config['API_BASE_URI'] . $this->config['API_VERSION'] . '/';
    }

    private function createEndpointUri($pathAsArray, $args, $inputParams)
    {
        $uri = '';
        foreach ($pathAsArray as $part) {
            if (in_array($part, array_keys($inputParams))) {
                $part = $inputParams[$part];
            }
            $uri .= "$part/";
        }
        if (! empty($args)) {
            $uri .= '?';
            foreach ($args as $arg) {
                $uri .= "$arg=$inputParams[$arg]";
            }
        }
        $token = (strpos('?', $uri) !== false) ? "&token=" : '?token=';
        $token .= env('IEX_API_PUBLIC_TOKEN');

        return $this->createBaseUri() . $uri . $token;
    }
    /*
     * modifies $pathAsArray by removing all ':'
     */
    private function getRequiredParams(&$pathAsArray, $args)
    {
        $pathArgs = [];
        foreach ($pathAsArray as $key => &$value) {
            if ($value == '') {
                unset($pathAsArray[$key]);
            }
            if (strpos($value, ':') !== false) {
                $value = str_replace(':', '', $value);
                $pathArgs[] = $value;
            }
        }
        return $requiredArgs = array_merge($pathArgs, $args);
    }

    private function checkIfAllParamsAreProvided($requiredArgs, $params)
    {
        if ($diff = array_diff(array_values($requiredArgs), array_keys($params))) {
            $missingArgs = '';
            foreach ($diff as $value) {
                $missingArgs .= $value . ', ';
            }
            $missingArgs = rtrim($missingArgs, ", ");
            throw new \Exception("Required parameters $missingArgs not found in provided parameters");
        }

        return true;
    }

    private function validateParams($params, $acceptedValues)
    {
        foreach ($params as $paramName => $paramValue) {
            $paramName = strtoupper($paramName);
            if (in_array(strtoupper($paramName), array_keys($acceptedValues))) {
                if (! in_array($paramValue, $acceptedValues[$paramName])) {
                    throw new \Exception("$paramName value of: $paramValue is not in accepted range defined in iex.php config file");
                }
            }
        }
        return $params;
    }
/* OLD IMPLEMENTATION remove after testing
    private function createUri($endPoint, $parameters)
    {
        $baseUri = self::API_BASE_URI . self::API_VERSION . '/';
        $token = '?token=' . env('IEX_API_PUBLIC_TOKEN');
        return $baseUri . $endPoint . $parameters . $token;
    }

    public function getDividendData($symbol, $range = '1m')
    {

        $cost = 10;

        if (!in_array($range, self::DIVIDEND_RANGES)) {
            throw new \Exception('Undefined range');
        }
        $endPoint = 'stock/' . $symbol . '/dividends/';
        $uri = $this->createUri($endPoint, $range);
        $data = file_get_contents($uri);
        $this->calculateCost($cost);

        return $data;
    }
*/
    public function __destruct()
    {
        $iexCost = new IexCost();
        $iexCost->cost = $this->cost;
        $iexCost->date = Carbon::today();
        $iexCost->save();
    }

    private function calculateCost($cost)
    {
        $this->cost += $cost;
    }

    private function getConfig($apiVersion = 'V1')
    {
        return config("iex.$apiVersion");
    }
}