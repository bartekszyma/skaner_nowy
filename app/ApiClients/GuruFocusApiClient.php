<?php
declare(strict_types=1);

namespace App\APIClients;


use App\APIClients\Exceptions\ApiClientException;
use App\APIClients\Exceptions\GuruFocusApiClientException;
use Illuminate\Http\JsonResponse;

class GuruFocusApiClient
{
    const CONFIG = 'api_clients.GuruFocus';
    private $baseUrl;
    private $token;

    public function __construct()
    {
        $this->baseUrl = config(self::CONFIG . '.base_url');
        $this->token = config(self::CONFIG . '.token');
    }

    public function getData(string $ticker, string $endpoint)
    {
        if (! in_array($endpoint, array_keys(config(self::CONFIG . '.endpoints')))) {
            throw new GuruFocusApiClientException('Unknown endpoint');
        }
        $url = $this->generateUrl($ticker, $endpoint);
        $response = null;
        try {
            $response = file_get_contents($url);
        } catch (\Throwable $t) {
            $msg = $t->getMessage();
            logger($msg);
        }

        return $response;
    }

    private function generateUrl(string $ticker, string $endpoint)
    {
        $url = $this->baseUrl . $this->token . config(self::CONFIG . '.endpoints.' . $endpoint);
        $url = trim(str_replace('{symbol?}', $ticker, $url));
        return $url;
    }

}