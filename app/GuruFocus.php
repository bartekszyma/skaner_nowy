<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuruFocus extends Model
{
    protected $table = 'guru_focus';

    public function getLastQuarterForFinancials($ticker)
    {
        return $this->where([['ticker', '=', $ticker], ['type', '=', 'financials']])->orderBy('last_quarter', 'DESC')->first();
    }

    public function getLastQuarterForEastimates($ticker) 
    {
        return $this->where([['ticker', '=', $ticker], ['type', '=', 'estimates']])->orderBy('last_quarter', 'DESC')->first();
    }

}
