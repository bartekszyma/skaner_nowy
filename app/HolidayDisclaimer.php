<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HolidayDisclaimer extends Model
{
    protected $table = 'holidays_disclaimers';
    const UPDATED_AT = null;
    const CREATED_AT = null;

    public function nyseHoliday()
    {
        return $this->belongsTo('NyseHoliday', 'disclaimer', 'id');
    }
}
