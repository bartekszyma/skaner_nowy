<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcelWorkshopsDataRequest extends Model
{
    protected $table = 'excel_workshops_data_requests';

    public function log($email, $ticker, $url)
    {
        $this->email = $email;
        $this->ticker = $ticker;
        $this->url = $url;
        $this->save();
    }
}
