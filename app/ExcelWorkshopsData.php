<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcelWorkshopsData extends Model
{
    protected $table = 'excel_workshops_data';

    public function getXYears(string $ticker, int $x = 6)
    {
        return $this->where('ticker', $ticker)->where('type', '10K')->take($x)->get()->sortBy('fy', false)->toArray();
    }

    public function getXQuarters(string $ticker, int $x = 24)
    {
        return $this->where('ticker', $ticker)->where('type', '10Q')->take($x)->get()->sortBy('fy', false)->toArray();
    }

    public function fromArray($financials, $estimates, $ticker, $period = 'annuals')
    {
        /*
         * well in this weired case PRU does not have financials key
         */

        $financials = $financials['financials'] ?? $financials;
        $incomeStatement = $financials[$period]['income_statement'];
        $balanceSheet = $financials[$period]['balance_sheet'];
        $perShareData = $financials[$period]['per_share_data_array'];
        $cashflowStatement = $financials[$period]['cashflow_statement'];
        $vluationAndQuality = $financials[$period]['valuation_and_quality'];
        $iterator = count($financials[$period]['Fiscal Year']) - 1;
        $type = $period === 'annuals' ? '10K' : '10Q';

        while ($iterator >= 0) {
            $x = new ExcelWorkshopsData();
            $x->type = $type;
            $x->ticker = $ticker;

            $x->revenue = $incomeStatement['Revenue'][$iterator] ?? 0;
            $x->ebitda = $incomeStatement['EBITDA'][$iterator] ?? 0;
            $x->fy = $financials[$period]['Fiscal Year'][$iterator] ?? 0;
            $x->interest_income = $incomeStatement['Interest Income'][$iterator] ?? 0;
            $x->interest_expense = $incomeStatement['Interest Expense'][$iterator] ?? 0;
            $x->EBIT = $incomeStatement['EBIT'][$iterator] ?? 0;
            $x->net_income = $incomeStatement['Net Income'][$iterator] ?? 0;
            $x->eps_basic = $incomeStatement['EPS (Diluted)'][$iterator] ?? 0;
            $x->eps_diluted = $incomeStatement['EPS (Diluted)'][$iterator] ?? 0;
            $x->eps_without_nri = $perShareData['EPS without NRI'][$iterator] ?? 0;
            $x->dividends_per_share = $perShareData['Dividends per Share'][$iterator] ?? 0;

            $x->cash_equivalents_and_marketable_securities =
                $balanceSheet['Cash, Cash Equivalents, Marketable Securities'][$iterator] ??
                /*
                 * banks
                 */
                $balanceSheet['Balance Statement Cash and cash equivalents'][$iterator] ?? 0;

            $x->total_inventories = $balanceSheet['Total Inventories'][$iterator] ?? 0;
            $x->total_current_assets = $balanceSheet['Total Current Assets'][$iterator] ?? 0;
            $x->total_assets = $balanceSheet['Total Assets'][$iterator] ?? 0;
            $x->short_term_debt = $balanceSheet['Short-Term Debt & Capital Lease Obligation'][$iterator] ?? 0;
            $x->long_term_debt = $balanceSheet['Long-Term Debt & Capital Lease Obligation'][$iterator] ?? 0;
            $x->total_debt = $x->short_term_debt + $x->long_term_debt ?? 0;
            $x->total_current_liabilities = $balanceSheet['Total Current Liabilities'][$iterator] ?? 0;
            $x->total_long_term_liabilities = $balanceSheet['Total Long-Term Liabilities'][$iterator] ?? 0;
            $x->total_liabilities = $balanceSheet['Total Liabilities'][$iterator] ?? 0;
            $x->total_equity = $balanceSheet['Total Equity'][$iterator] ?? 0;
            $x->shares_outstanding_diluted = $incomeStatement['Shares Outstanding (Diluted Average)'][$iterator] ?? 0;
            $x->cashflow_from_operations = $cashflowStatement['Cash Flow from Operations'][$iterator] ?? 0;
            $x->cashflow_from_investing = $cashflowStatement['Cash Flow from Investing'][$iterator] ?? 0;
            $x->issuance_of_stocks = $cashflowStatement['Issuance of Stock'][$iterator] ?? 0;
            $x->repurchase_of_stocks = $cashflowStatement['Repurchase of Stock'][$iterator] ?? 0;
            $x->net_stocks_repurchase = $x->issuance_of_stocks + $x->repurchase_of_stocks ?? 0;
            $x->net_issuance_of_debt = $cashflowStatement['Net Issuance of Debt'][$iterator] ?? 0;
            $x->cashflow_from_financing = $cashflowStatement['Cash Flow from Financing'][$iterator] ?? 0;
            $x->month_end_stock_price = $vluationAndQuality['Month End Stock Price'][$iterator] ?? 0;

            if ($x->eps_basic === 0 || $x->eps_basic ==='-' || $x->net_income_ex_nri === null) {
                $x->net_income_ex_nri = 0;
            } else {
                $x->net_income_ex_nri = $x->eps_diluted / $x->eps_basic * $x->net_income;
            }

            $x->estimate_eps1 = $estimates['per_share_eps_estimate'][0] ?? null;
            $x->estimate_eps2 = $estimates['per_share_eps_estimate'][1] ?? null;
            $x->estimate_eps3 = $estimates['per_share_eps_estimate'][2] ?? null;

            $x->estimate_date1 = isset($estimates['date'][0]) ? substr($estimates['date'][0], 0, 4) . '-' . substr($estimates['date'][0], 4, 2) : null;
            $x->estimate_date2 = isset($estimates['date'][1]) ? substr($estimates['date'][1], 0, 4) . '-' . substr($estimates['date'][1], 4, 2) : null;
            $x->estimate_date3 = isset($estimates['date'][2]) ? substr($estimates['date'][2], 0, 4) . '-' . substr($estimates['date'][2], 4, 2) : null;

            $x->free_cash_flow = $cashflowStatement['Free Cash Flow'][$iterator] ?? 0;

            $values = array_keys($x->getAttributes());

            foreach ($values as $value) {

                if ($x->$value === '-') {
                    $x->$value = 0;
                }
            }
            $x->save();

            $iterator--;
        }
    }
}
