<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => '/scanner'], function () {
        Route::get('/', 'ScannerController@index')->name('home');
        Route::get('/filter', 'ScannerController@filter')->name('scanner-filter');
        Route::get('/filter/results', 'ScannerController@getFilteredData')->name('scanner-filter-results');
        Route::get('/charts', 'ScannerController@charts')->name('scanner-charts');
    });
//    Route::get('/scanner', 'ScannerController@index')->name('home');
//    Route::get('/scanner/filter', 'ScannerController@filter')->name('scanner-filter');
//    Route::get('/scanner/filter/results', 'ScannerController@getFilteredData')->name('scanner-filter-results');
//    Route::get('/scanner/charts', 'ScannerController@charts')->name('scanner-charts');

    Route::group(['prefix' => '/dividends'], function () {
        Route::get('/calendar', 'GrowingDividendsPortfolioController@index')->name('growingDividends:calendar');
        Route::get('/portfolio', 'GrowingDividendsPortfolioController@index')->name('growingDividends:portfolio');
        Route::get('/timeline', 'GrowingDividendsPortfolioController@timeline')->name('growingDividends:timeline');
    });

    Route::get('/toggle', 'TestController@toggleDemo')->name('toggle-demo');

    Route::group(['prefix' => '/accountant'], function () {
        Route::get('/', 'Accountant\ProcessFileController@load');
    });
});

Route::get('/', function () {
    return redirect(route('login'));
});

Route::get('/newsletter', function () {
    return view('newsletter');
});

Route::get('/test/{ticker}', 'TestController@index');
Route::get('data/{ticker}', 'DataController@index');


Route::get('/data/{token}', 'DataController@token');

Route::get('/data/get-gf/{ticker}', 'GuruFocusController@getFinancials');
//Route::get('/data/show/{ticker}', 'DataController@showData')->name('show-excel-data');
