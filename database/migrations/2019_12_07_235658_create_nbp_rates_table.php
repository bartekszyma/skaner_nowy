<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNbpRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nbp_rates', function (Blueprint $table) {
            $table->date('date');
            $table->float('USD');
            $table->float('EUR');
            $table->float('GBP');
            $table->float('CAD');
            $table->float('CHF');
            $table->float('AUD');
            $table->float('NOK');
            $table->float('JPY');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nbp_rates');
    }
}
