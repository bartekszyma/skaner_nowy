<?php
/**
 * each scenario defines which endpoints should be asked for data
 * should be defined both in api_client and in scrapper config files
 */

return [
    'GuruFocus' => [
        'scenarios' => [
            'scrap_financials' => [
                'financials',
                'estimates'
            ]
        ]
    ]
];