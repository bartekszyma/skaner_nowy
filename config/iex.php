<?php

return [
    "V1" =>
        [
            'API_VERSION' => 'V1',
            'API_BASE_URI' => 'https://cloud.iexapis.com/',
            'DOCUMENTATION' => 'https://iexcloud.io/docs/api/', /* each endpoint ends with #endpoint_name ie. #dividends */
            'ENDPOINTS' =>
                [
                    'DIVIDENDS' =>
                        [
                            'URI' =>
                                [
                                    'METHOD' => 'GET',
                                    'PATH' => '/stock/:symbol/dividends/:range',
                                    'ARGS' => /*only for endpoint key=value*/
                                        [],
                                    'ALLOWED_ARGS_VALUES' =>
                                        [
                                            'RANGE' => [
                                                '5y',
                                                '2y',
                                                '1y',
                                                'ytd',
                                                '6m',
                                                '3m',
                                                '1m',
                                                'next'
                                            ],
                                        ],
                                    'DEFAULT_ARGS_VALUES' =>
                                        [],
                                    'COST' => 10,
                                ],
                        ],
                    'EARNINGS' =>
                        [
                            /*
                             Available Methods
                                GET /stock/{symbol}/earnings
                                GET /stock/{symbol}/earnings/{last}
                                GET /stock/{symbol}/earnings/{last}/{field}
                                GET /stock/{symbol}/earnings/{last}?period=annual
                             */
                            'URI' =>
                                [
                                    'METHOD' => 'GET',
                                    /*
                                     * last: Optional, number. Number of quarters or years to return. Default is 1.
                                     */
                                    'PATH' => '/stock/:symbol/earnings/:last',
                                    'ARGS' => /*only for endpoint key=value*/
                                        [
                                            'period=annual'
                                        ],
                                    'ALLOWED_ARGS_VALUES' =>
                                        [
                                            'PERIOD' =>
                                                [
                                                    'annual',
                                                    'quarter'
                                                ],
                                            'LAST' =>
                                                [
                                                    1,
                                                    2,
                                                    3,
                                                    4
                                                ]
                                        ],
                                    'DEFAULT_ARGS_VALUES' =>
                                        [
                                            'PERIOD' => 'quarter',
                                            'LAST' => 1
                                        ],
                                    'COST' => 1000,
                                ],
                        ],
                ],
        ],
];