<?php

return [
    'GuruFocus' =>
    [
        /*
         * https://api.gurufocus.com/public/user/token//stock/slg/analyst_estimate
         */
        'base_url' => 'https://api.gurufocus.com/public/user/',
        'token' => 'tutaj wpisz swój token',
        'endpoints' => [
            'financials' => '/stock/{symbol?}/financials ',
            'estimates' => '/stock/{symbol?}/analyst_estimate'
        ],
    ]
];